angular.module("app").controller("loginCtrl",["$http","$scope","$state","authService",function(s,a,e,t){t.clearAll();var r=new Date;a.year=r.getFullYear(),a.css={submit:"btn-primary"},a.data={username:"",password:""},a.doLogin=function(){var r=new jsSHA(a.data.password,"TEXT");s.post("/api/sessions",{username:a.data.username,password:r.getHash("SHA-512","HEX")}).success(function(s){t.setSID(s.response.token),$.localStorage.set("username",a.data.username),a.css.submit="btn-success",e.go("app.quotes")}).error(function(s){$("#user").focus(),a.data.password="",a.css.submit="btn-danger"})}}]);