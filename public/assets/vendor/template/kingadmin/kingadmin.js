$(document).ready(function() {
  /**
   * @return {undefined}
   */
  function update() {
    if ($(window).width() < 977) {
      if ($(".left-sidebar").hasClass("minified")) {
        $(".left-sidebar").removeClass("minified");
        $(".left-sidebar").addClass("init-minified");
      }
    } else {
      if ($(".left-sidebar").hasClass("init-minified")) {
        $(".left-sidebar").removeClass("init-minified").addClass("minified");
      }
    }
  }
  /**
   * @param {Object} parent
   * @param {?} node
   * @return {undefined}
   */
  function parse(parent, node) {
    $(".widget-ajax .alert").removeClass("alert-info").addClass("alert-success").find("span").text(node);
    $(".widget-ajax .alert").find("i").removeClass().addClass("fa fa-check-circle");
    parent.prop("disabled", false);
    parent.find("i").removeClass().addClass("fa fa-floppy-o");
    parent.find("span").text("Update");
  }
  /**
   * @return {undefined}
   */
  function animate() {
    if (3 > a) {
      $globalVolume = localStorage.getItem("global-volume");
      if (null == $globalVolume || "1" == $globalVolume) {
        player.play();
      }
      /** @type {number} */
      interval = setTimeout(animate, 8E3);
      a++;
    }
  }
  /**
   * @param {HTMLElement} name
   * @param {string} c
   * @return {undefined}
   */
  function addClass(name, c) {
    if (null == c || "1" == c) {
      name.removeClass("fa-volume-off").addClass("fa-volume-up");
    } else {
      name.removeClass("fa-volume-up").addClass("fa-volume-off");
    }
  }
  if ($(".main-menu .js-sub-menu-toggle").click(function(types) {
    types.preventDefault();
    $li = $(this).parents("li");
    if ($li.hasClass("active")) {
      $li.find(".toggle-icon").removeClass("fa-angle-down").addClass("fa-angle-left");
      $li.removeClass("active");
    } else {
      $li.find(".toggle-icon").removeClass("fa-angle-left").addClass("fa-angle-down");
      $li.addClass("active");
    }
    $li.find(".sub-menu").slideToggle(300);
  }), $(".js-toggle-minified").clickToggle(function() {
    $(".left-sidebar").addClass("minified");
    $(".content-wrapper").addClass("expanded");
    $(".left-sidebar .sub-menu").css("display", "none").css("overflow", "hidden");
    $(".sidebar-minified").find("i.fa-angle-left").toggleClass("fa-angle-right");
  }, function() {
    $(".left-sidebar").removeClass("minified");
    $(".content-wrapper").removeClass("expanded");
    $(".sidebar-minified").find("i.fa-angle-left").toggleClass("fa-angle-right");
  }), $(".main-nav-toggle").clickToggle(function() {
    $(".left-sidebar").slideDown(300);
  }, function() {
    $(".left-sidebar").slideUp(300);
  }), $mainContentCopy = $(".main-content").clone(), $('.searchbox input[type="search"]').keydown(function() {
    var $radio = $(this);
    setTimeout(function() {
      var trigger = $radio.val();
      if (trigger.length > 2) {
        /** @type {RegExp} */
        var stringPrefixes = new RegExp(trigger, "i");
        /** @type {Array} */
        var corners = [];
        $(".widget-header h3").each(function() {
          var val = $(this).text().match(stringPrefixes);
          if ("" != val) {
            if (null != val) {
              corners.push($(this).parents(".widget"));
            }
          }
        });
        if (corners.length > 0) {
          $(".main-content .widget").hide();
          $.each(corners, function(dataAndEvents, a) {
            a.show();
          });
        } else {
          console.log("widget not found");
        }
      } else {
        $(".main-content .widget").show();
      }
    }, 0);
  }), $(".widget .btn-remove").click(function(types) {
    types.preventDefault();
    $(this).parents(".widget").fadeOut(300, function() {
      $(this).remove();
    });
  }), $(".widget .btn-toggle-expand").clickToggle(function(types) {
    types.preventDefault();
    $(this).parents(".widget").find(".widget-content").slideUp(300);
    $(this).find("i.fa-chevron-up").toggleClass("fa-chevron-down");
  }, function(types) {
    types.preventDefault();
    $(this).parents(".widget").find(".widget-content").slideDown(300);
    $(this).find("i.fa-chevron-up").toggleClass("fa-chevron-down");
  }), $(".widget .btn-focus").clickToggle(function(types) {
    types.preventDefault();
    $(this).find("i.fa-eye").toggleClass("fa-eye-slash");
    $(this).parents(".widget").find(".btn-remove").addClass("link-disabled");
    $(this).parents(".widget").addClass("widget-focus-enabled");
    $('<div id="focus-overlay"></div>').hide().appendTo("body").fadeIn(300);
  }, function(types) {
    types.preventDefault();
    $theWidget = $(this).parents(".widget");
    $(this).find("i.fa-eye").toggleClass("fa-eye-slash");
    $theWidget.find(".btn-remove").removeClass("link-disabled");
    $("body").find("#focus-overlay").fadeOut(function() {
      $(this).remove();
      $theWidget.removeClass("widget-focus-enabled");
    });
  }), $(window).bind("resize", update), $("body").tooltip({
    selector : "[data-toggle=tooltip]",
    container : "body"
  }), $(".alert .close").click(function(types) {
    types.preventDefault();
    $(this).parents(".alert").fadeOut(300);
  }), $(".btn-help").popover({
    container : "body",
    placement : "top",
    html : true,
    title : '<i class="fa fa-book"></i> Help',
    content : "Help summary goes here. Options can be passed via data attributes <code>data-</code> or JavaScript. Please check <a href='http://getbootstrap.com/javascript/#popovers'>Bootstrap Doc</a>"
  }), $(".demo-popover1 #popover-title").popover({
    html : true,
    title : '<i class="fa fa-cogs"></i> Popover Title',
    content : "This popover has title and support HTML content. Quickly implement process-centric networks rather than compelling potentialities. Objectively reinvent competitive technologies after high standards in process improvements. Phosfluorescently cultivate 24/365."
  }), $(".demo-popover1 #popover-hover").popover({
    html : true,
    title : '<i class="fa fa-cogs"></i> Popover Title',
    trigger : "hover",
    content : "Activate the popover on hover. Objectively enable optimal opportunities without market positioning expertise. Assertively optimize multidisciplinary benefits rather than holistic experiences. Credibly underwhelm real-time paradigms with."
  }), $(".demo-popover2 .btn").popover(), $(".widget-header-toolbar .btn-ajax").click(function(types) {
    types.preventDefault();
    $theButton = $(this);
    $.ajax({
      url : "php/widget-ajax.php",
      type : "POST",
      dataType : "json",
      cache : false,
      /**
       * @return {undefined}
       */
      beforeSend : function() {
        $theButton.prop("disabled", true);
        $theButton.find("i").removeClass().addClass("fa fa-spinner fa-spin");
        $theButton.find("span").text("Loading...");
      },
      /**
       * @param {Object} o
       * @return {undefined}
       */
      success : function(o) {
        setTimeout(function() {
          parse($theButton, o.msg);
        }, 1E3);
      },
      /**
       * @param {?} textStatus
       * @param {?} jqXHR
       * @param {string} msg
       * @return {undefined}
       */
      error : function(textStatus, jqXHR, msg) {
        console.log("AJAX ERROR: \n" + msg);
      }
    });
  }), $(".widget-header .multiselect").length > 0 && $(".widget-header .multiselect").multiselect({
    dropRight : true,
    buttonClass : "btn btn-warning btn-sm"
  }), $(".today-reminder").length > 0) {
    var interval;
    /** @type {number} */
    var a = 0;
    var player = new Audio;
    /** @type {string} */
    player.src = navigator.userAgent.match("Firefox/") ? "assets/audio/bell-ringing.ogg" : "assets/audio/bell-ringing.mp3";
    animate();
  }
  if ($(".bs-switch").length > 0) {
    $(".bs-switch").bootstrapSwitch();
  }
  if ($(".demo-only-page-blank").length > 0) {
    $(".content-wrapper").css("min-height", $(".wrapper").outerHeight(true) - $(".top-bar").outerHeight(true));
  }
  if ($(".top-general-alert").length > 0) {
    if (null == localStorage.getItem("general-alert")) {
      $(".top-general-alert").delay(800).slideDown("medium");
      $(".top-general-alert .close").click(function() {
        $(this).parent().slideUp("fast");
        localStorage.setItem("general-alert", "closed");
      });
    }
  }
  $btnGlobalvol = $(".btn-global-volume");
  $theIcon = $btnGlobalvol.find("i");
  addClass($theIcon, localStorage.getItem("global-volume"));
  $btnGlobalvol.click(function() {
    var zeroQuoted = localStorage.getItem("global-volume");
    if (null == zeroQuoted || "1" == zeroQuoted) {
      localStorage.setItem("global-volume", 0);
    } else {
      localStorage.setItem("global-volume", 1);
    }
    addClass($theIcon, localStorage.getItem("global-volume"));
  });
}), $.fn.clickToggle = function(matcherFunction, _fn) {
  return this.each(function() {
    /** @type {boolean} */
    var o = false;
    $(this).bind("click", function() {
      return o ? (o = false, _fn.apply(this, arguments)) : (o = true, matcherFunction.apply(this, arguments));
    });
  });
};
