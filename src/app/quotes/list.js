angular.module('app').controller(
    'quotesCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        clients,
        ngTableParams,
        quote_status,
        quotes
    ) {

        $scope.data   = quotes.collection;
        $scope.status = quote_status.collection;
        $scope.client = {};

        $scope.check  = [];
        $scope.q      = [];
        $scope.filter = {
            status : [],
            date : {
                from : null,
                to : null
            },
            date_payment : {
                from : null,
                to : null
            },
        };

        /**
         * Prepare data
         */

        $scope.addToolTip = function (row) {
            var tooltip = angular.copy(row.client);

            if (row.client_name !== null && row.client_name.length > 0) {
                tooltip += '<br>'+angular.copy(row.client_name);
            }

            if (row.client_phone !== null && row.client_phone.length > 0) {
                tooltip += '<br>'+angular.copy(row.client_phone);
            }

            if (row.client_phone2 !== null && row.client_phone2.length > 0) {
                tooltip += '<br>'+angular.copy(row.client_phone2);
            }

            if (row.client_address !== null && row.client_address.length > 0) {
                tooltip += '<br>'+angular.copy(row.client_address);
            }

            return tooltip;
        };

        for (var i = $scope.data.length - 1; i >= 0; i--) {
            $scope.data[i].tooltip = $scope.addToolTip($scope.data[i]);
        };

        for (var i = $scope.status.length - 1; i >= 0; i--) {
            if ($scope.status[i].show_in_list == 1) {
                $scope.filter.status.push($scope.status[i].id_quote_status);
                $scope.check[$scope.status[i].id_quote_status] = true;
            } else {
                $scope.check[$scope.status[i].id_quote_status] = false;
            }
        }

        /**
         * Watchers
         */

        $scope.$watch('data', function() {
            $timeout(function () {
                $scope.tableParams.reload();
            });
        }, true);

        $scope.$watch('filter.date', function () {
            $scope.tableParams.reload();
        }, true);

        $scope.$watch('filter.date_payment', function () {
            $scope.tableParams.reload();
        }, true);

        /**
         * Table
         */

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {sequence : 'desc'}
        }, {
            getData : function($defer, params) {
                var data   = $scope.data;
                var filter = params.filter() ? params.filter() : [];
                for (var i = filter.length - 1; i >= 0; i--) {
                    data = $filter('filter')(data, filter[i]);
                }

                data = $filter('quoteDateFtr')(data, 'dt_create', $scope.filter.date);
                data = $filter('quoteDateFtr')(data, 'dt_payment', $scope.filter.date_payment);
                data = $filter('quoteStatusFtr')(data, $scope.filter.status);
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                params.total(data.length);
                $timeout(function(){
                    $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        });

        $scope.toggleStatus = function (value) {
            var id = $scope.filter.status.indexOf(value);

            if (id > -1) {
                $scope.filter.status.splice(id, 1);
            } else {
                $scope.filter.status.push(value);
            }

            $scope.tableParams.reload();
        };

        $scope.doClientEdit = function (row) {
            if (row) {
                $scope.client = {};
                $scope.client.id_user = row.client_id;
                $scope.client.id_user_type = '587434b4056f2914ce7f2e29';
                $scope.client.username = row.client;
                $scope.client.name = row.client_name;
                $scope.client.address = row.client_address;
                $scope.client.phone = row.client_phone;
                $scope.client.phone2 = row.client_phone2;
                $scope.client.title = 'Modificar';
            } else {
                $scope.client = {};
                $scope.client.id_user_type = '587434b4056f2914ce7f2e29';
                $scope.client.username = null;
                $scope.client.name = null;
                $scope.client.address = null;
                $scope.client.phone = null;
                $scope.client.phone2 = null;
                $scope.client.title = 'Agregar';
            }

            $('#modalClientEdit').modal();
        };

        $scope.doClientSave = function () {
            clients.save($scope.client).then(function () {
                for (var i = $scope.data.length - 1; i >= 0; i--) {
                    if ($scope.data[i].client_id == $scope.client.id_user) {
                        $scope.data[i].client = $scope.client.username;
                        $scope.data[i].client_name = $scope.client.name;
                        $scope.data[i].client_address = $scope.client.address;
                        $scope.data[i].client_phone = $scope.client.phone;
                        $scope.data[i].client_phone2 = $scope.client.phone2;
                        $scope.data[i].tooltip = $scope.addToolTip($scope.data[i]);
                    }
                };
                toastr.success('Registro guardado');
            });
        };
});