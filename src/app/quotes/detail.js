angular.module('app').controller(
    'quoteDetailCtrl',
    function (
        $filter,
        $http,
        $scope,
        $state,
        $timeout,
        authService,
        clients,
        collection,
        files,
        ngTableParams,
        suppliers,
        quotes,
        quote_conditions,
        quote_log,
        quote_parts,
        quote_receipts,
        quote_status,
        ASSETS
    ) {
        // Catalogos
        $scope.quote     = quotes.collection[0];
        $scope.suppliers = suppliers.collection;
        $scope.part_no   = quote_parts.collection;
        $scope.receipts  = quote_receipts.collection;
        $scope.id_quote  = quotes.collection[0].id_quote; // use when searching for quote id
        $scope.sequence  = quotes.collection[0].sequence;
        $scope.subject   = {
            prefix : 'Quote #S'+$scope.quote.id_quote,
            placeholder : $scope.quote.subject,
            edit : ''
        };

        $scope.client = {};
        $scope.files1 = [];
        $scope.files2 = [];
        $scope.files3 = [];
        $scope.files4 = [];
        $scope.files4 = [];
        $scope.files5 = [];
        $scope.files6 = [];

        $scope.token = authService.getSID();
        $scope.logs = quote_log.collection;
        $scope.conditions = quote_conditions.collection;
        $scope.status = quote_status.collection;
        $scope.isValid = true;
        $scope.error = [];

        //quote
        $scope.part_no.subtotal = 0;
        $scope.part_no.total = 0;
        $scope.part_no.total_shipping = 0;
        $scope.part_no.tax_shipping = 0;
        $scope.part_no.change_value = 0;

        // Seleccionado
        $scope.edit = {};
        $scope.supplier = {};
        $scope.resend_supplier = {
            emails : [],
            data : [],
            disabled : true
        };

        $scope.part_no.phone = "USA || +1 (469) 458-1746 || Toll free:1 844-540-4707";

        $scope.part_no.phones = [
            'USA || +1 (469) 458-1746 || Toll free:1 844-540-4707', 
            'Mexico || +52 (81) 1936-4316'
        ];

        $scope.part_no.address = [
            'Meditegic, LLC || EIN: 98-1278579 || Mapcode: P70VM.2Q54', 
            'Meditegic, SA de CV || RFC: MED151007HIA || Mapcode: P6VFD.GF20'
        ];

        $scope.part_no.adds = "Meditegic, LLC || EIN: 98-1278579 || Mapcode: P70VM.2Q54";
       
        $scope.part_no.change = "Pesos";

        $scope.part_no.changes = [
            'Pesos',
            'Dolares',
        ];



        $scope.init = function () {
            toastr.clear();

            $scope.client = {
                id_user : clients.collection[0].id_user,
                id_user_type : clients.collection[0].id_user_type,
                username : clients.collection[0].username,
                name : clients.collection[0].name,
                address : clients.collection[0].address,
                phone : clients.collection[0].phone,
                phone2 : clients.collection[0].phone2,
                tooltip : clients.getToolTip(clients.collection[0])
            };

            for (var i = files.collection.length - 1; i >= 0; i--) {
                fileCollection = $scope.collectionFileType(files.collection[i].id_file_type);
                files.collection[i].url = ASSETS.apiURL + files.collection[i].url;
                $scope[fileCollection].push(files.collection[i]);
            };

            $scope.getLogs();

            /**
             * Dropzone config
             */

            var dropzone_options = {
                addRemoveLinks : true,
                clickable : true,
                parallelUploads : 10,
                url : '/api/files'
            };

            var dropzone_events = {
                success : function (file, data) {
                    $scope.$apply(function () {
                        fileCollection = $scope.collectionFileType(data.response.id_file_type);
                        $scope[fileCollection].push(data.response);
                        toastr.success('Archivo guardado');
                        $scope.getLogs();
                    });
                }
            };

            $scope.dropzone_config = {
                options : dropzone_options,
                eventHandlers : dropzone_events
            };
        };

        // Watchers

       /* $scope.$watch('part_no', function () {
            console.log("entro");
            // $scope.quote.total_purchase = 0;
            // $scope.quote.total_shipping = 0;
            // $scope.quote.total_expense = 0;
            // $scope.quote.total_refound = 0;
            // $scope.quote.total_income = 0;

            // var part_no = {};
            // for (var i = $scope.part_no.length - 1; i >= 0; i--) {
            //     $scope.part_no[i].total_expense = 0;
            //     if (!isNaN(parseFloat($scope.part_no[i].total_purchase))) {
            //         $scope.part_no[i].total_expense += parseFloat($scope.part_no[i].total_purchase);
            //         $scope.quote.total_purchase += parseFloat($scope.part_no[i].total_purchase);
            //         $scope.quote.total_expense += parseFloat($scope.part_no[i].total_purchase);
            //     }
            //     if (!isNaN(parseFloat($scope.part_no[i].total_shipping))) {
            //         $scope.part_no[i].total_expense += parseFloat($scope.part_no[i].total_shipping);
            //         $scope.quote.total_shipping += parseFloat($scope.part_no[i].total_shipping);
            //         $scope.quote.total_expense += parseFloat($scope.part_no[i].total_purchase);
            //     }

            //     // Suggested sale price
            //     $scope.part_no[i].suggest_income = $scope.part_no[i].total_expense + ($scope.part_no[i].total_expense * 0.3);

            //     var income = (!isNaN(parseFloat($scope.part_no[i].total_income))) ? parseFloat($scope.part_no[i].total_income) : $scope.part_no[i].suggest_income;

            //     $scope.part_no[i].total_utility = income - $scope.part_no[i].total_expense;
            //     $scope.quote.total_income += income;

            //     $scope.part_no[i].class = null;
            //     if ($scope.part_no[i].total_utility < 0) {
            //         $scope.part_no[i].class = 'danger';
            //         $scope.isValid = false;
            //     } else {
            //         $scope.isValid = true;
            //     }

            //     // Refounds
            //     if (!isNaN(parseFloat($scope.part_no[i].total_refound)) && $scope.part_no[i].condition == 7) {
            //         $scope.quote.total_refound += parseFloat($scope.part_no[i].total_refound);
            //     }
            // };

            $scope.calUtility();
        }, true);*/
        // $scope.$watch('quote.size' , function () {
        //     $scope.calSubTotal();
        // }, true);

        // $scope.$watch('quote.total_income', function () {
        //     // $scope.calSubTotal();
        // });

        // $scope.$watch('quote.total_other', function () {
        //     // $scope.calSubTotal();
        // });

        // Totals

        /*$scope.calUtility = function (item) {
            if (typeof item == "undefined") {
                return false;
            }
            if () {

            }
            // var purchase = $scope.quote.total_purchase;
            // var income   = $scope.quote.total_income;
            // var shipping = $scope.quote.total_shipping;
            // var other    = (parseFloat($scope.quote.total_other) > 0) ? parseFloat($scope.quote.total_other) : 0;
            // var refound  = $scope.quote.total_refound;
            // var expense  = purchase + shipping + other + refound;

            // $scope.quote.total_expense = expense;
            // $scope.quote.total_utility = income - expense;

            // if (expense == 0 && income == 0) {
            //     $scope.quote.utility_per = 0;
            // } else if (expense == 0) {
            //     $scope.quote.utility_per = 100;
            // } else if (expense == income) {
            //     $scope.quote.utility_per = 100;
            // }  else if (expense > income) {
            //     $scope.quote.utility_per = 0;
            // } else {
            //     $scope.quote.utility_per = (income - expense) / expense * 100;
            // }
        };*/
        $scope.getPhone = function(){
            var phone   = $scope.part_no.phone;
            var address = $scope.part_no.adds;
        }

        $scope.getTypeChange = function(){
            var typeChange = $scope.part_no.change;
            /*
            if (change==="Dolares"){
                $('#change_value').removeClass('hidden');
            } else {
                $('#change_value').addClass('hidden');
            }
            */
        }

        $scope.exChange = function(){
            $scope.part_no.change_value = ($scope.part_no.change_value || 0);
            var change = $scope.part_no.change_value;
        }

        $scope.calSubTotal = function()
        {
            var total = 0;
            var sub = 0;
            var tax_total_env = 0;

            for (var i = $scope.part_no.length - 1; i >= 0; i--) {
                var item = $scope.part_no[i];
                var total = 0;
                var tax = 0;

                item.size = (item.size || 1);
                var utility = item.total_purchase * (item.utility_per / 100);
                utility = (utility || 0);
                total = (item.size * item.total_purchase) + (utility * item.size);
                total = (total || 0);
                if (item.tax) {
                    tax = total * (item.tax/100);
                }
                item.total = total+tax;
                sub += (item.total || 0);
            }
            $scope.part_no.subtotal       = sub;
            $scope.part_no.tax_shipping   = ($scope.part_no.tax_shipping || 0);
            $scope.part_no.total_shipping = ($scope.part_no.total_shipping || 0);

            tax_total_env                     = (($scope.part_no.total_shipping / 100) * $scope.part_no.tax_shipping);
            $scope.part_no.tax_shipping_total = tax_total_env + $scope.part_no.total_shipping;
            $scope.part_no.total              = sub + $scope.part_no.total_shipping + tax_total_env;
        }

        $scope.calTotalInput = function(item)
        {
            var tax = 0;
            var size = (item.size || 1);
            var total = (size * item.total_purchase);
            if (item.tax) {
                tax = item.total * (item.tax/100);
            }
            var rest = (item.total - tax) - total;
            var porcent = rest /total;
            item.utility_per = porcent * 100;
            setTimeout(function(){ $scope.calSubTotal(); }, 100);
        }

        /**
         * File
         */

        $scope.collectionFileType = function (id_file_type) {
            switch (id_file_type) {
                case '58787e29056f29085b4bf702':
                    return 'files1';
                case '58787e29056f29085b4bf703':
                    return 'files2';
                case '58787e29056f29085b4bf704':
                    return 'files3';
                case '58787e29056f29085b4bf705':
                    return 'files4';
                case '58787e29056f29085b4bf706':
                    return 'files5';
                case '58787e29056f29085b4bf707':
                    return 'files6';
            }
        };

        $scope.deleteFile = function (row) {
            $scope.edit = row;
            $('#modealDeleteFile').modal();
        };

        $scope.doRemoveFile = function () {
            var config = {
                method : 'DELETE',
                url : '/api/files/' + $scope.edit.id_file
            };

            $http(config)
                .success(function (data) {
                    fileCollection = $scope.collectionFileType($scope.edit.id_file_type);
                    collection.remove($scope[fileCollection], $scope.edit.id_file, 'id_file');
                    toastr.success('Registro eliminado');
                    $scope.edit = {};
                    $scope.getLogs();
                });
        };

        // Part no

        $scope.addPartNo = function () {
            $scope.edit = {};
            $('#modalAddPartNo').modal();
        };

        $scope.doAddPartNo = function () {
            var config = {
                data : {part_no : $scope.edit.part_no},
                method : 'POST',
                url : '/api/quotes/' + $scope.quote.id_quote + '/part_no'
            };

            $http(config)
                .success(function (data) {
                    collection.add($scope.part_no, data.response, 'id_quote_part');
                    toastr.success('Registro guardado');
                    $scope.edit = {};
                    $scope.getLogs();
                });
        };

        // Logs

        $scope.getLogs = function () {
            var config = {
                method : 'GET',
                url    : '/api/quotes/' + $scope.quote.id_quote + '/logs'
            };

            $http(config)
                .success(function (data) {
                    $scope.logs = [];
                    for (var i = data.response.length - 1; i >= 0; i--) {
                        data.response[i].dt_hint = moment(data.response[i].dt_create, 'YYYY-MM-DD HH:mm:ss').fromNow();
                        $scope.logs.unshift(data.response[i]);
                    }
                });
        }

        // Save

        $scope.doSaveQuote = function () {
            var config = {
                data   : $scope.quote,
                method : 'PUT',
                url    : '/api/quotes/' + $scope.quote.id_quote
            };

            if ($scope.subject.edit.length > 0) {
                $scope.quote.subject = angular.copy($scope.subject.prefix)+' - '+angular.copy($scope.subject.edit);
                $scope.subject.placeholder = angular.copy($scope.quote.subject);
            }

            $http(config)
                .success(function (data) {
                    $scope.subject.edit = '';
                    $scope.doSavePartNo();
                });
        }

        $scope.doSavePartNo = function () {
            var config = {
                data : {part_no : $scope.part_no},
                method : 'PUT',
                url : '/api/quotes/' + $scope.quote.id_quote + '/part_no'
            };

            $http(config)
                .success(function (data) {
                    toastr.success('Registro guardado');
                    $scope.getLogs();
                });
        }

        $scope.options = {
            barColor : 'green'
        };

        /**
         * Returns
         */

        $scope.doCountReturns = function () {
            var count = 0;
            for (var i = $scope.part_no.length - 1; i >= 0; i--) {
                if ($scope.part_no[i].condition == 7) {
                    count++;
                }
            };

            return count;
        }

        $scope.doGetReturns = function () {
            var count = 0;
            for (var i = $scope.part_no.length - 1; i >= 0; i--) {
                if ($scope.part_no[i].condition == 7) {
                    count++;
                }
            };

            return count;
        }

        $scope.searchQoute = function () {
            if ($scope.sequence != quotes.collection[0].sequence) {
                quotes.get({sequence : $scope.sequence}).then(function (res) {
                    if (res.data.response.length === 0) {
                        toastr.warning('Cotización <b>#S'+$scope.sequence+'</b> no encontrada');
                    } else {
                        toastr.success('Cargando cotización <b>#S'+$scope.sequence+'</b>');
                        $state.go('app.quoteDetail', {id : res.data.response[0].id_quote});
                    }
                });
            } else {
                toastr.info('Cotización <b>#S'+$scope.sequence+'</b> ya esta cargada');
            }
        }

        /**
         * Receipts
         */

        var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' + '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

        var render = {
            item: function(item, escape) {
                var label = '<div>';
                label += (item.text) ? '<b>' + escape(item.text) + '</b> ' : '' ;
                label += escape(item.value)
                label += '</div>';
                return label;
            },
            option: function (item, escape) {
                var label = '<div>';
                label += (item.text) ? '<b>' + escape(item.text) + '</b> ' : '' ;
                label += escape(item.value)
                label += '</div>';
                return label;
            }
        };

        var createFilter = function (input) {
            var match, regex;

            regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
            match = input.match(regex);
            if (match) return !this.options.hasOwnProperty(match[0]);

            regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
            match = input.match(regex);
            if (match) return !this.options.hasOwnProperty(match[2]);

            return false;
        };

        var create = function (input) {
            if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                return {
                    value: input
                };
            }
        };

        var selectize = $('#receipt').selectize({
            maxItems     : 1,
            searchField  : ['text', 'value'],
            render       : render,
            createFilter : createFilter,
            create       : create
        });

        var select_receipt = selectize[0].selectize;

        $scope.$watch('resend_supplier.data', function () {
            $scope.resend_supplier.disabled = ($scope.resend_supplier.data.length == 0) ? true : false;
        }, true);

        $scope.doResendSuppliers = function () {
            $scope.resend_supplier.emails = [];
            for (var i = $scope.receipts.length - 1; i >= 0; i--) {
                for (var j = $scope.resend_supplier.data.length - 1; j >= 0; j--) {
                    if ($scope.receipts[i].id_quote_receipt == $scope.resend_supplier.data[j]) {
                        $scope.resend_supplier.emails.push($scope.receipts[i].email);
                    }
                };
            };
            $('#modalResendReceipt').modal();
        };

        $scope.doResendSuppliersAll = function() {
            for (var i = $scope.receipts.length - 1; i >= 0; i--) {
                $scope.resend_supplier.data.push($scope.receipts[i].id_quote_receipt);
            };
        };

        $scope.doResendSuppliersClear = function() {
            $scope.resend_supplier.data = [];
        };

        $scope.clearReceipt = function () {
            $scope.supplier = {
                email: '',
                send: 1
            };

            $timeout(function() {
                select_receipt.clear();
                select_receipt.clearOptions();

                for (var i = $scope.suppliers.length - 1; i >= 0; i--) {
                    var found = false;

                    for (var j = $scope.receipts.length - 1; j >= 0; j--) {
                        if ($scope.receipts[j].email == $scope.suppliers[i].username) {
                            found = true;
                        }
                    }

                    if (!found) {
                        select_receipt.addOption({
                            text  : $scope.suppliers[i].username,
                            value : $scope.suppliers[i].username
                        });
                    }
                }
            });
        };

        $scope.modalAddSupplier = function () {
            $scope.clearReceipt();
            $('#modalAddSupplier').modal();
        };

        $scope.modalDeleteSupplier = function (receipt) {
            $scope.supplier = receipt;
            $('#modalDeleteSupplier').modal();
        };

        $scope.doAddSupplier = function () {
            $scope.supplier.send = parseInt($scope.supplier.send);

            var config = {
                data : $scope.supplier,
                method : 'POST',
                url : '/api/quotes/' + $scope.quote.id_quote + '/receipts'
            };

            $http(config)
                .success(function (data) {
                    collection.add($scope.receipts, data.response, 'id_quote_receipt');
                    toastr.success('Registro guardado');
                    $scope.clearReceipt();
                    $scope.getLogs();
                });
        };

        $scope.doResend = function () {
            var config = {
                data   : {id_quote_receipt : $scope.resend_supplier.data},
                method : 'POST',
                url    : '/api/quotes/' + $scope.quote.id_quote + '/resend'
            };

            $http(config)
                .success(function (data) {
                    toastr.success('Cotización enviada');
                    $scope.resend_supplier.data = [];
                    $scope.getLogs();
                });
        };

        $scope.doRemoveSupplier = function () {
            var config = {
                method : 'DELETE',
                url    : '/api/quotes/' + $scope.quote.id_quote + '/receipts/' + $scope.supplier.id_quote_receipt
            };

            $http(config)
                .success(function (data) {
                    collection.remove($scope.receipts, $scope.supplier.id_quote_receipt, 'id_quote_receipt');
                    toastr.success('Registro eliminado');
                    $scope.clearReceipt();
                    $scope.getLogs();
                });
        };

        /**
         * Client
         */

        $scope.doClientEdit = function () {
            $('#modalClientEdit').modal();
        };

        $scope.doClientSave = function () {
            clients.save($scope.client).then(function (res) {
                $scope.client.tooltip = clients.getToolTip($scope.client);

                toastr.success('Registro guardado');
            });
        };

        // Init
        $scope.init();
});
