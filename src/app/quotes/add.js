angular.module('app').controller(
    'addQuotesCtrl',
    function (
        $http,
        $scope,
        $timeout,
        clients,
        collection,
        groups,
        next,
        suppliers,
        templates,
        quotes,
        users_groups
    ) {

    $scope.attachments = [];
    $scope.client = {};
    $scope.group = {};
    $scope.selected = {};
    $scope.created = {};
    $scope.now = new Date();

    // Init

    $scope.init = function () {
        $scope.doClear();

        $scope.dropzoneConfig1 = {
            options : {
                addRemoveLinks : true,
                parallelUploads : 10,
                clickable : true,
                url : '/api/files'
            },
            eventHandlers : {
                removedfile : function(file) {
                    if (file.id_file) {
                        $http.delete('/api/files/' + file.id_file)
                            .success(function (data) {
                                collection.remove($scope.selected.attachments, file.id_file, 'id_file');
                            });
                    }
                },
                sending : function(file, xhr, formData) {
                    formData.append('id_file_type', '58787e29056f29085b4bf704');
                },
                success : function (file, data) {
                    file.id_file = data.response.id_file;
                    $scope.$apply(function () {
                        $scope.selected.attachments.push({
                            id_file : data.response.id_file,
                            name : data.response.name,
                        });
                    });
                }
            }
        };
    };

    // Watchers

    $scope.$watch('selected.textangular', function () {
        if (!angular.equals(templates.collection[0].markup, $scope.selected.textangular)) {
            $scope.selected.body = $scope.selected.textangular;
        }
    });

    $scope.doClear = function () {
        $scope.selected = {
            next : next,
            subject : '',
            body : '',
            textangular : templates.collection[0].markup,
            to : [],
            client : '',
            part_no : [],
            manual : [],
            attachments : []
        };

        $scope.group = {
            selected : [],
            rows : []
        };

        $timeout(function() {
            select_client.clear();
            select_group.clear();
            select_to.clear();
        });

        quotes.next().then(function (res) {
            $scope.selected.next = res.data.response;
        });
    };

    $scope.doAddGroup = function () {
        if ($scope.group.selected.length === 0) {
            return;
        }

        var row = {
            id_groups : angular.copy($scope.group.selected),
            emails    : [],
            groups    : []
        };

        for (var i = 0; i < $scope.group.selected.length; i++) {
            // Set group names
            for (var k = 0; k < groups.collection.length; k++) {
                if ($scope.group.selected[i] == groups.collection[k].id_group) {
                    row.groups.push(groups.collection[k].name);
                }
            }
        }


        // Matches groups with usernames
        users_groups.match({id_group: $scope.group.selected.join()}).then(function (res) {
            for (var i = 0; i < res.data.response.length; i++) {
                row.emails.push(res.data.response[i].username);
            };

            $scope.group.selected = [];
            $scope.group.rows.push(row);

            $timeout(function() {
                select_group.clear();
                // Adds emails
                for (var i = 0; i < row.emails.length; i++) {
                    select_to.addItem(row.emails[i]);
                };
            });
        });
    };

    $scope.doGetEmails = function () {
        $scope.selected.to = [];
        $timeout(function() {
            select_to.clear();

            for (var i = 0; i < $scope.selected.manual.length; i++) {
                $scope.selected.to.push($scope.selected.manual[i]);
                select_to.addItem($scope.selected.manual[i]);
            }
            for (var i = 0; i < $scope.group.rows.length; i++) {
                for (var j = 0; j < $scope.group.rows[i].emails.length; j++) {
                    var found = false;
                    for (var k = 0; k < $scope.selected.to.length; k++) {
                        if ($scope.group.rows[i].emails[j] == $scope.selected.to[k]) {
                            found = true;
                        }
                    }
                    if (!found) {
                        $scope.selected.to.push($scope.group.rows[i].emails[j]);
                        select_to.addItem($scope.group.rows[i].emails[j]);
                    }
                }
            }
        });
    };

    $scope.doPreview = function () {
        quotes.preview($scope.selected.body).then(function (res) {
            $scope.selected.part_no = res.data.response;

            if ($scope.selected.part_no.length === 0) {
                $scope.form2.$valid = false;
            } else {
                $scope.form2.$valid = true;
                $('#modalPreview').modal();
                $('#myTab a:first').tab('show');
            }
        });
    };

    $scope.doRemove = function (key) {
        $scope.group.rows.splice(key, 1);
        $scope.doGetEmails();
    };

    $scope.doSend = function () {
        var quote = {
            body : $scope.selected.body,
            client : ($scope.selected.client == '') ? null : $scope.selected.client,
            part_no : $scope.selected.part_no,
            subject : $scope.selected.subject,
            to : $scope.selected.to,
            files : $scope.selected.attachments
        };

        quotes.save(quote).then(function (res) {
            $scope.created = res.data.response;
            toastr.success('Registro guardado');

            location.reload();
        });
    };

    /**
     * Groups
     */

    var selectize = $('#groups').selectize({
        maxItems : null
    });

    var select_group = selectize[0].selectize;

    // Create options
    for (var i = 0; i < groups.collection.length; i++) {
        select_group.addOption({
            text  : groups.collection[i].name,
            value : groups.collection[i].id_group
        });
    }

    /**
     * Clients
     */

    var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' + '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

    var render = {
        item: function(item, escape) {
            var label = '<div>';
            label += (item.text) ? '<b>' + escape(item.text) + '</b> ' : '' ;
            label += escape(item.value)
            label += '</div>';
            return label;
        },
        option: function (item, escape) {
            var label = '<div>';
            label += (item.text) ? '<b>' + escape(item.text) + '</b> ' : '' ;
            label += escape(item.value)
            label += '</div>';
            return label;
        }
    };

    var createFilter = function (input) {
        var match, regex;

        regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
        match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[0]);

        regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
        match = input.match(regex);
        if (match) return !this.options.hasOwnProperty(match[2]);

        return false;
    };

    var create = function (input) {
        if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
            return {
                value: input
            };
        }
    };

    var selectize = $('#client').selectize({
        maxItems     : 1,
        searchField  : ['text', 'value'],
        render       : render,
        createFilter : createFilter,
        create       : create
    });

    var select_client = selectize[0].selectize;

    // Create options
    for (var i = 0; i < clients.collection.length; i++) {
        select_client.addOption({
            text  : clients.collection[i].name,
            value : clients.collection[i].username
        });
    };

    $scope.doClientEdit = function (username) {
        var found = false;

        for (var i = clients.collection.length - 1; i >= 0; i--) {
            if (clients.collection[i].username === username) {
                $scope.client = {};
                $scope.client.id_user = clients.collection[i].id_user;
                $scope.client.id_user_type = '587434b4056f2914ce7f2e29';
                $scope.client.username = clients.collection[i].username;
                $scope.client.name = clients.collection[i].name;
                $scope.client.address = clients.collection[i].address;
                $scope.client.phone = clients.collection[i].phone;
                $scope.client.phone2 = clients.collection[i].phone2;
                $scope.client.title = 'Modificar';

                found = true;
            }
        }

        if (!found) {
            $scope.client = {};
            $scope.client.id_user_type = '587434b4056f2914ce7f2e29';
            $scope.client.username = username;
            $scope.client.name = null;
            $scope.client.address = null;
            $scope.client.phone = null;
            $scope.client.phone2 = null;
            $scope.client.title = 'Agregar';
        }

        $('#modalClientEdit').modal();
    };

    $scope.doClientSave = function () {
        clients.save($scope.client).then(function (res) {
            console.info
            var found = false;

            for (var i = clients.collection.length - 1; i >= 0; i--) {
                if (clients.collection[i].id_user == res.data.response.id_user) {
                    clients.collection[i].id_user = res.data.response.id_user;
                    clients.collection[i].username = res.data.response.username;
                    clients.collection[i].name = res.data.response.name;
                    clients.collection[i].address = res.data.response.address;
                    clients.collection[i].phone = res.data.response.phone;
                    clients.collection[i].phone2 = res.data.response.phone2;

                    found = true;
                }
            };

            if (!found) {
                clients.collection.push({
                    id_user : res.data.response.id_user,
                    username : res.data.response.username,
                    name : res.data.response.name,
                    phone : res.data.response.phone,
                    phone2 : res.data.response.phone2
                });
            }

            select_client.updateOption(res.data.response.username, {
                text  : res.data.response.name,
                value : res.data.response.username
            });

            toastr.success('Registro guardado');
        });
    };

    /**
     * Receipts
     */

    var selectize = $('#to').selectize({
        maxItems     : null,
        searchField  : ['text', 'value'],
        render       : render,
        createFilter : createFilter,
        create       : create,
        onItemAdd    : function (input) {
            var found = false;
            for (var i = 0; i < $scope.group.rows.length; i++) {
                for (var j = 0; j < $scope.group.rows[i].emails.length; j++) {
                    if ($scope.group.rows[i].emails[j] == input) {
                        found = true;
                    }
                }
            }
            if (!found) {
                for (var i = 0; i < $scope.selected.manual.length; i++) {
                    if ($scope.selected.manual[i] == input) {
                        found = true;
                    }
                }
            }
            // email was added manually
            if (!found) {
                $scope.selected.manual.push(input);
            }
        },
        onItemRemove : function (input) {
            for (var i = 0; i < $scope.selected.manual.length; i++) {
                if ($scope.selected.manual[i] == input) {
                    $scope.selected.manual.splice(i, 1);
                }
            }
        }
    });

    var select_to = selectize[0].selectize;

    // Create options
    for (var i = 0; i < suppliers.collection.length; i++) {
        select_to.addOption({
            text  : suppliers.collection[i].name,
            value : suppliers.collection[i].username
        });
    }

    /**
     * Init
     */

    $scope.init();
});
