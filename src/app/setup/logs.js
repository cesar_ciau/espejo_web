angular.module('app').controller(
    'logsCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        logs,
        logs_uuid,
        ngTableParams
    ) {
        $scope.data = logs_uuid.collection;
        $scope.data2 = [];
        $scope.log = null;
        $scope.detail = null;
        $scope.q = [];

        $scope.$watch('data', function(data) {
            $timeout(function () {
                for (var i = data.length - 1; i >= 0; i--) {
                    if (data[i].code === null) {
                        continue;
                    }

                    var code = data[i].code.toString();
                    switch(code.substring(0, 1)) {
                        case '1':
                            data[i].label = 'label-primary';
                            break;
                        case '2':
                            data[i].label = 'label-success';
                            break;
                        case '4':
                            data[i].label = 'label-warning';
                            break;
                        case '5':
                            data[i].label = 'label-danger';
                            break;
                        default:
                            data[i].label = 'label-default';
                            break;
                    }
                }
                $scope.tableParams.reload();
            });
        }, true);

        $scope.$watch('data2', function() {
            $timeout(function () {
                $scope.tableParams2.reload();
            });
        }, true);

        $scope.doDetail = function (row) {
            $scope.detail = row;
            $('#modalDetail').modal();
        };

        $scope.doSelect = function (row) {
            $scope.log = row;
            $('#myTab a[href="#tab1"]').tab('show');
            $http.get('/api/logs?id_log_uuid='+row.id_log_uuid).then(function (res) {
                for (var i = res.data.response.length - 1; i >= 0; i--) {
                    switch(res.data.response[i].level) {
                        case 'notice':
                            res.data.response[i].label = 'label-success';
                            break;
                        case 'debug':
                            res.data.response[i].label = 'label-default';
                            break;
                        case 'info':
                            res.data.response[i].label = 'label-info';
                            break;
                        case 'alert':
                        case 'warning':
                            res.data.response[i].label = 'label-warning';
                            break;
                        case 'error':
                        case 'emergency':
                        case 'critical':
                            res.data.response[i].label = 'label-danger';
                            break;
                    }
                }
                $scope.data2 = res.data.response;
            });
        };

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {dt_create : 'desc'}
        }, {
            getData : function($defer, params) {
                var _data   = $scope.data;
                var _filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < _filter.length; ++key) {
                    _data = $filter('filter')(_data, _filter[key]);
                }
                _data = params.sorting() ? $filter('orderBy')(_data, params.orderBy()) : _data;
                params.total(_data.length);
                $defer.resolve(_data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.tableParams2 = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q2,
            sorting : {count : 'asc'}
        }, {
            getData : function($defer, params) {
                var _data   = $scope.data2;
                var _filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < _filter.length; ++key) {
                    _data = $filter('filter')(_data, _filter[key]);
                }
                _data = params.sorting() ? $filter('orderBy')(_data, params.orderBy()) : _data;
                params.total(_data.length);
                $defer.resolve(_data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
});
