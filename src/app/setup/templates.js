angular.module('app').controller(
    'templatesCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        locales,
        ngTableParams,
        templates,
        template_types
    ) {

        $scope.data = templates.collection;
        $scope.template_types = template_types.collection;
        $scope.locales = locales.collection;

        $scope.q = [];
        $scope.edit = {};
        $scope.select = [];
        $scope.preview = '';

        $scope.$watch('data', function() {
            $timeout(function () {
                $scope.tableParams.reload();
            });
        }, true);

        $scope.doEdit = function (row) {
            $scope.edit = {};
            if (row) {
                $scope.edit = angular.copy(row);
                $scope.edit.title = 'Modificar';
            } else {
                $scope.edit.title = 'Agregar';
            }
            $('#modalEdit').modal();
        };

        $scope.doSave = function () {
            templates.save($scope.edit).then(function () {
                toastr.success('Registro guardado');
            });
        };

        $scope.doConfirm = function (row) {
            $scope.edit = row;
            $('#modalConfirm').modal();
        };

        $scope.doPreview = function (row) {
            $scope.edit = row;
            $scope.doReloadPreview();
            $('#modalPreview').modal();
        };

        $scope.doReloadPreview = function () {
            $scope.preview = '<i class="fa fa-refresh fa-spin fa-fw"></i> Refrescando';

            $http.get('/api/templates/'+$scope.edit.id_template+'/preview').then(function(res) {

                if (res.data.response) {
                    $scope.preview = res.data.response;
                }
            });
        };

        $scope.doRemove = function () {
            templates.delete($scope.edit[templates.id()]).then(function () {
                toastr.success('Registro eliminado');
            });
        };

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {'name' : 'asc'}
        }, {
            getData : function($defer, params) {
                var data   = $scope.data;
                var filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < filter.length; ++key) {
                    data = $filter('filter')(data, filter[key]);
                }
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                params.total(data.length);
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
});
