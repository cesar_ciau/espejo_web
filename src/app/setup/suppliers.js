angular.module('app').controller(
    'suppliersCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        groups,
        ngTableParams,
        users,
        users_groups
    ) {

        $scope.data = users.collection;
        $scope.groups = groups.collection;
        $scope.users_groups = users_groups.collection;
        $scope.options = [];

        $scope.q = [];
        $scope.edit = {};
        $scope.select = [];

        $scope.$watch('data', function() {
            $timeout(function () {
                $scope.tableParams.reload();
            });
        }, true);

        $scope.init = function () {
            // Load multiselect
            for (var i = $scope.groups.length - 1; i >= 0; i--) {
                $scope.options.push({
                    value : $scope.groups[i].id_group,
                    text : $scope.groups[i].name
                });
            };

            // Add group for each user
            for (var key = 0; key < $scope.data.length; ++key) {
                $scope.data[key].groups = [];
                $scope.data[key].select = [];

                // Adds to select
                for (var key2 = 0; key2 < $scope.users_groups.length; ++key2) {
                    if ($scope.users_groups[key2].id_user == $scope.data[key].id_user) {
                        $scope.data[key].select.push(String($scope.users_groups[key2].id_group));
                    }
                }

                // Adds users
                $scope.data[key] = $scope.addGroupName($scope.data[key]);
            }
        };

        $scope.addGroupName = function (row) {
            row.groups = [];

            for (var key2 = 0; key2 < row.select.length; ++key2) {
                for (var key3 = 0; key3 < $scope.groups.length; ++key3) {
                    if (row.select[key2] == $scope.groups[key3].id_group) {
                        row.groups.push($scope.groups[key3].name);
                    }
                }
            }

            return row;
        };

        $scope.doEdit = function (row) {
            if (row) {
                $scope.edit = angular.copy(row);
                $scope.edit.title = 'Modificar';
            } else {
                $scope.edit = {};
                $scope.edit.id_user_type = '587434b4056f2914ce7f2e28';
                $scope.edit.username     = null;
                $scope.edit.name         = null;
                $scope.edit.phone        = null;
                $scope.edit.phone2       = null;
                $scope.edit.groups       = [];
                $scope.edit.select       = [];
                $scope.edit.title = 'Agregar';
            }
            if ($scope.edit.select.length > 0) {
                $('#multiSelect').multiSelect('select_all');
                $('#multiSelect').multiSelect('deselect_all');
                $('#multiSelect').multiSelect('select', $scope.edit.select);
            }

            $('#modalEdit').modal();
        };

        $scope.doSave = function () {
            users.save($scope.edit).then(function (res) {
                // Overwrite previous edit
                $scope.edit.id_user = res.data.response.id_user;

                // Clear previous groups
                users_groups.delete($scope.edit[users.id()]).then(function () {
                    // Save new groups
                    for (var i = 0; i < $scope.edit.select.length; i++) {
                        var params = {
                            id_user_group : null,
                            id_user  : $scope.edit[users.id()],
                            id_group : $scope.edit.select[i]
                        };

                        users_groups.save(params).then(function () {
                            for (var i = $scope.data.length - 1; i >= 0; i--) {
                                if ($scope.edit.id_user == $scope.data[i].id_user) {
                                    $scope.data[i].groups = angular.copy($scope.edit.groups);
                                    $scope.data[i].select = angular.copy($scope.edit.select);
                                }
                            };

                            toastr.success('Registro guardado');
                        });
                    }
                });
            });
        };

        $scope.doConfirm = function (row) {
            $scope.edit = row;
            $('#modalConfirm').modal();
        };

        $scope.doRemove = function () {
            users.delete($scope.edit[users.id()]).then(function () {
                users_groups.delete($scope.edit[users.id()]).then(function () {
                    toastr.success('Registro eliminado');
                });
            });
        };

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {id_user:'asc'}
        }, {
            getData : function($defer, params) {
                var data   = $scope.data;
                var filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < filter.length; ++key) {
                    data = $filter('filter')(data, filter[key]);
                }
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                params.total(data.length);
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        /**
         * Init
         */

        $scope.init();
});
