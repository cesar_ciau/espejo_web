angular.module('app').controller(
    'labelsCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        labels,
        locales,
        ngTableParams
    ) {

        $scope.data = labels.collection;
        $scope.locales = locales.collection;

        $scope.q = [];
        $scope.edit = {};
        $scope.select = [];

        $scope.$watch('data', function() {
            $timeout(function () {
                $scope.tableParams.reload();
            });
        }, true);

        $scope.doEdit = function (row) {
            $scope.edit = {};
            if (row) {
                $scope.edit = angular.copy(row);
                $scope.edit.title = 'Modificar';
            } else {
                $scope.edit.title = 'Agregar';
            }
            $('#modalEdit').modal();
        };

        $scope.doSave = function () {
            labels.save($scope.edit).then(function () {
                toastr.success('Registro guardado');
            });
        };

        $scope.doConfirm = function (row) {
            $scope.edit = row;
            $('#modalConfirm').modal();
        };

        $scope.doRemove = function () {
            labels.delete($scope.edit[labels.id()]).then(function () {
                toastr.success('Registro eliminado');
            });
        };

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {name : 'asc'}
        }, {
            getData : function($defer, params) {
                var _data   = $scope.data;
                var _filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < _filter.length; ++key) {
                    _data = $filter('filter')(_data, _filter[key]);
                }
                _data = params.sorting() ? $filter('orderBy')(_data, params.orderBy()) : _data;
                params.total(_data.length);
                $defer.resolve(_data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
});
