angular.module('app').controller(
    'usersCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        ngTableParams,
        users
    ) {

        $scope.data = users.collection;
        $scope.q = [];
        $scope.edit = {};
        $scope.select = [];

        $scope.$watch('data', function() {
            $timeout(function () {
                $scope.tableParams.reload();
            });
        }, true);

        $scope.doEdit = function (row) {
            if (row) {
                $scope.edit = angular.copy(row);

                $scope.edit.password = null;
                $scope.edit.title    = 'Modificar';
            } else {
                $scope.edit = {};

                $scope.edit.id_user_type = '587434b4056f2914ce7f2e27';
                $scope.edit.username     = null;
                $scope.edit.password     = null;
                $scope.edit.name         = null;
                $scope.edit.title        = 'Agregar';
            }

            $('#modalEdit').modal();
        };

        $scope.doSave = function () {
            users.save($scope.edit).then(function () {
                toastr.success('Registro guardado');
            });
        };

        $scope.doConfirm = function (row) {
            $scope.edit = row;
            $('#modalConfirm').modal();
        };

        $scope.doRemove = function () {
            users.delete($scope.edit[users.id()]).then(function () {
                toastr.success('Registro eliminado');
            });
        };

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {'id_user' : 'asc'}
        }, {
            getData : function($defer, params) {
                var data   = $scope.data;
                var filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < filter.length; ++key) {
                    data = $filter('filter')(data, filter[key]);
                }
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                params.total(data.length);
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
});
