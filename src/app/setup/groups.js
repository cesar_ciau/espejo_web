angular.module('app').controller(
    'groupsCtrl',
    function (
        $filter,
        $http,
        $scope,
        $timeout,
        groups,
        ngTableParams,
        users,
        users_groups
    ) {

        $scope.data = groups.collection;
        $scope.users = users.collection;
        $scope.users_groups = users_groups.collection;

        $scope.q = [];
        $scope.edit = {};
        $scope.select = [];
        $scope.options = [];

        $scope.$watch('data', function() {
            $timeout(function () {
                $scope.tableParams.reload();
            });
        }, true);

        $scope.init = function () {
            // Load multiselect
            for (var i = $scope.users.length - 1; i >= 0; i--) {
                $scope.options.push({
                    value : $scope.users[i].id_user,
                    text : $scope.users[i].username
                });
            };

            // Add group for each user
            for (var key = 0; key < $scope.data.length; ++key) {
                $scope.data[key].users = [];
                $scope.data[key].select = [];

                // Adds to select
                for (var key2 = 0; key2 < $scope.users_groups.length; ++key2) {
                    if ($scope.users_groups[key2].id_group == $scope.data[key].id_group) {
                        $scope.data[key].select.push(String($scope.users_groups[key2].id_user));
                    }
                }

                // Adds users
                $scope.data[key] = $scope.addUsers($scope.data[key]);
            }
        };

        $scope.addUsers = function (row) {
            row.users = [];

            for (var key2 = 0; key2 < row.select.length; ++key2) {
                for (var key3 = 0; key3 < $scope.users.length; ++key3) {
                    if (row.select[key2] == $scope.users[key3].id_user) {
                        row.users.push($scope.users[key3].username);
                    }
                }
            }

            return row;
        };

        $scope.doEdit = function (row) {
            if (row) {
                $scope.edit = angular.copy(row);
                $scope.edit.title = 'Modificar';
            } else {
                $scope.edit = {};
                $scope.edit.name   = null;
                $scope.edit.users  = [];
                $scope.edit.select = [];
                $scope.edit.title = 'Agregar';
            }

            $('#multiSelect').multiSelect('deselect_all');
            $('#multiSelect').multiSelect('select', $scope.edit.select);

            $('#modalEdit').modal();
        };

        $scope.doSave = function () {
            groups.save($scope.edit).then(function (res) {
                // Overwrite previous edit
                $scope.edit.id_group = res.data.response.id_group;

                // Clear previous groups
                users_groups.delete(null, $scope.edit[groups.id()]).then(function () {
                    // Save new groups
                    for (var i = 0; i < $scope.edit.select.length; i++) {
                        var params = {
                            id_user_group : null,
                            id_user  : $scope.edit.select[i],
                            id_group : $scope.edit[groups.id()]
                        };

                        users_groups.save(params).then(function () {
                            for (var i = $scope.data.length - 1; i >= 0; i--) {
                                if ($scope.edit.id_group == $scope.data[i].id_group) {
                                    $scope.data[i].users = angular.copy($scope.edit.users);
                                    $scope.data[i].select = angular.copy($scope.edit.select);
                                }
                            };
                        });
                    }

                    toastr.success('Registro guardado');
                });
            });
        };

        $scope.doConfirm = function (row) {
            $scope.edit = row;
            $('#modalConfirm').modal();
        };

        $scope.doRemove = function () {
            groups.delete($scope.edit[groups.id()]).then(function () {
                toastr.success('Registro eliminado');
            });
        };

        $scope.tableParams = new ngTableParams({
            page    : 1,
            count   : 50,
            filter  : $scope.q,
            sorting : {name:'asc'}
        }, {
            getData : function($defer, params) {
                var data   = $scope.data;
                var filter = params.filter() ? params.filter() : [];
                for (var key = 0; key < filter.length; ++key) {
                    data = $filter('filter')(data, filter[key]);
                }
                data = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                params.total(data.length);
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        // Init
        $scope.init();
});
