angular.module('app').controller(
    'defaultCtrl',
    function (
        $http,
        $scope,
        $state
    ) {

    $scope.username = $.localStorage.get('username');

    var d = new Date();
    $scope.year = d.getFullYear();

    $scope.menuItems = [
        {
            'text' : 'Inicio',
            'icon' : 'fa-home',
            'state' : 'app.home',
            'class' : 'open',
            'children' : null
        },
        {
            'text' : 'Cotizaciones',
            'icon' : 'fa-calculator',
            'state' : 'app.home',
            'class' : 'open',
            'children' : [
                {
                    'text' : 'Agregar',
                    'icon' : 'fa-edit',
                    'state' : 'app.addQuote',
                    'class' : ''
                },
                {
                    'text' : 'Listado',
                    'icon' : 'fa-table',
                    'state' : 'app.quotes',
                    'class' : ''
                }
            ]
        },
        {
            'text' : 'Configuración',
            'icon' : 'fa-gears',
            'state' : 'app.home',
            'class' : 'open',
            'children' : [
                {
                    'text' : 'Usuarios',
                    'icon' : 'fa-user',
                    'state' : 'app.users',
                    'class' : ''
                },
                {
                    'text' : 'Clientes',
                    'icon' : 'fa-briefcase',
                    'state' : 'app.clients',
                    'class' : ''
                },
                {
                    'text' : 'Proveedores',
                    'icon' : 'fa-truck',
                    'state' : 'app.suppliers',
                    'class' : ''
                },
                {
                    'text' : 'Grupos',
                    'icon' : 'fa-users',
                    'state' : 'app.groups',
                    'class' : ''
                },
                {
                    'text' : 'Plantillas',
                    'icon' : 'fa-file-text-o',
                    'state' : 'app.templates',
                    'class' : ''
                },
                {
                    'text' : 'Etiquetas',
                    'icon' : 'fa-language',
                    'state' : 'app.labels',
                    'class' : ''
                }
            ]
        }
    ];

    $scope.doLogout = function () {
        $http
            .delete('/api/sessions', {})
            .success(function(data) {
                $state.go('login');
            });
    };

    // Toastr
    toastr.options = {
        closeButton     : true,
        debug           : false,
        positionClass   : 'toast-bottom-full-width',
        onclick         : null,
        showDuration    : '150',
        hideDuration    : '1000',
        timeOut         : '5000',
        extendedTimeOut : '1000',
        showEasing      : 'swing',
        hideEasing      : 'linear',
        showMethod      : 'fadeIn',
        hideMethod      : 'fadeOut'
    };

    // Momentjs
    moment.locale('es');

    // Tooltip (future tooltips)
    $(document.body).css('background-color', '#ececec');
    $(document.body).tooltip({html: true, selector: '[data-toggle="tooltip"]'});
});
