angular.module('app').directive(
    'dropzone',
    function (authService, ASSETS) {
        return function (scope, element, attrs) {
            Dropzone.autoDiscover = false;

            var config, dropzone;

            config = scope[attrs.dropzone];

            var parseURL = config.options.url.split('/');
            if (parseURL.length >= 1 && parseURL[1] === 'api') {
                parseURL.splice(1, 1);
                config.options.url = ASSETS.apiURL + parseURL.join('/');
            }
            config.options.headers = {};
            if (authService.getSID()) {
                config.options.headers['Authorization'] = authService.getSID();
            }
            config.options.headers['X-Requested-With'] = 'XMLHttpRequest';

            dropzone = new Dropzone(element[0], config.options);

            angular.forEach(config.eventHandlers, function (handler, event) {
                dropzone.on(event, handler);
            });
        };
});
