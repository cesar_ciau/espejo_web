angular.module('app').directive(
    'multiSelect',
    function ($timeout) {
        return {
            template : '<select id="multiSelect" multiple="multiple" name="selectGroup[]"></select>',
            scope : {options : '=', selected : '='},
            link : function (scope, element, attrs) {

                if (!angular.isArray(scope.selected)) {
                    scope.selected = [];
                }

                if (!angular.isArray(scope.options)) {
                    scope.options = [];
                }

                // Init object
                $('#multiSelect').multiSelect({
                    selectableHeader : '<input type="text" autocomplete="off" class="form-control" style="width:100%; margin-bottom:10px" placeholder="Buscar">',
                    selectionHeader : '<input type="text" autocomplete="off" class="form-control" style="width:100%; margin-bottom:10px" placeholder="Buscar">',
                    afterInit : function (ms) {
                        var that = this,
                        $selectableSearch      = that.$selectableUl.prev(),
                        $selectionSearch       = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString  = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = $selectableSearch.quicksearch(selectableSearchString).on('keydown', function(e){
                            if (e.which === 40){
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                        that.qs2 = $selectionSearch.quicksearch(selectionSearchString).on('keydown', function(e){
                            if (e.which == 40){
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
                    },
                    afterSelect : function (values) {
                        var found = false;
                        for (var key = 0; key < scope.selected.length; ++key) {
                            if (values[0] == scope.selected[key]) {
                                found = true;
                            }
                        }
                        if (!found) {
                            scope.selected.push(values[0]);
                        }
                        this.qs1.cache();
                        this.qs2.cache();
                        $timeout(function () {
                            scope.$apply(scope.selected);
                        });
                    },
                    afterDeselect : function (values) {
                        if (values === null) {
                            return;
                        }
                        for (var key = 0; key < scope.selected.length; ++key) {
                            if (values[0] == scope.selected[key]) {
                                scope.selected.splice(key, 1);
                            }
                        }
                        this.qs1.cache();
                        this.qs2.cache();
                        $timeout(function () {
                            scope.$apply(scope.selected);
                        });
                    }
                });

                for (var i = scope.options.length - 1; i >= 0; i--) {
                    $('#multiSelect').multiSelect('addOption', {
                        text: scope.options[i].text,
                        value: scope.options[i].value
                    });
                };
            }
        };
});
