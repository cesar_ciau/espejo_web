angular.module('app').directive(
    'datePicker',
    function ($timeout) {
        return {
            scope : {'data' : '='},
            link : function (scope, element, attrs) {
                scope.data = (scope.data) ? moment(scope.data * 1000) : null;

                $(element).datetimepicker({
                    format : "DD-MM-YYYY",
                    defaultDate : scope.data,
                    locale : moment.locale(),
                    showClear : true,
                    showClose : true,
                    useCurrent : false
                }).on('dp.change', function (event) {
                    scope.data = (event.date) ? event.date : null;
                    $timeout(function () {
                        scope.$apply(scope.data);
                    });
                });
            }
        };
});
