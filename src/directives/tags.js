angular.module('app').directive(
    'tags',
    function (
        $ocLazyLoad,
        $timeout
    ) {
        return {
            scope : {'data' : '='},
            link  : function (scope, element, attrs) {

                if (!angular.isArray(scope.data)) {
                    scope.data = [];
                }

                $(element).tagsInput({
                    height      : 47,
                    width       : '100%',
                    defaultText : '',
                    onAddTag    : function (elem) {
                        scope.data.push(elem);
                        scope.$apply(scope.data);
                    },
                    onRemoveTag : function (elem) {
                        for (var key = 0; key < scope.data.length; ++key) {
                            if (elem === scope.data[key]) {
                                scope.data.splice(key, 1);
                            }
                        }
                        $timeout(function () {
                            scope.$apply(scope.data);
                        });
                    }
                }).importTags(scope.data.toLocaleString());
            }
        };
});
