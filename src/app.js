/**
 * Config
 */

angular.module('app').run([
    '$http',
    '$rootScope',
    '$state',
    'authService',
    function (
        $http,
        $rootScope,
        $state,
        authService
    ) {
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            var authenticate = ('authenticate' in toState) ? toState.authenticate : true;

            if (authenticate && authService.isAuthenticated()) {
                $http.get('/api/sessions');
            } else if (authenticate && !authService.isAuthenticated()) {
                event.preventDefault();
                $state.go('login');
            }

            if (authenticate && typeof validateToken === 'undefined') {
                validateToken = setInterval(function () {
                    $http.get('/api/sessions');
                }, 60000);
            } else if (!authenticate && typeof validateToken !== 'undefined') {
                clearInterval(validateToken);
            }
        });
}]);

angular.module('app').config([
    '$httpProvider',
    '$locationProvider',
    '$stateProvider',
    '$urlRouterProvider',
    function (
        $httpProvider,
        $locationProvider,
        $stateProvider,
        $urlRouterProvider
    ) {

        $locationProvider.html5Mode({
            enabled : true,
            requireBase : false
        });

        $stateProvider
            .state('login', {
                authenticate : false,
                url : '/login',
                templateUrl : '/partials/login/index.html',
                resolve : {
                    load : function ($ocLazyLoad, ASSETS) {
                        return $ocLazyLoad.load([
                            ASSETS.jsSHA,
                            '/partials/login/index.js',
                            ]);
                    }
                },
                controller : 'loginCtrl'
            })
            .state('app', {
                abstract : true,
                url : '/app',
                templateUrl : '/partials/app/index.html',
                resolve : {
                    load : function ($ocLazyLoad, ASSETS) {
                        return $ocLazyLoad.load([
                            '/partials/app/index.js',
                            ]);
                    }
                }
            })
            .state('app.home', {
                url : '/home',
                templateUrl : '/partials/app/home/list.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load('/partials/app/home/list.js');
                    }
                },
                controller : 'homeCtrl'
            })
            .state('app.addQuote', {
                url : '/quotes/add',
                templateUrl : '/partials/app/quotes/add.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/dropzone.js',
                            '/partials/services/clients.js',
                            '/partials/services/groups.js',
                            '/partials/services/quotes.js',
                            '/partials/services/users.js',
                            '/partials/services/users_groups.js',
                            '/partials/services/suppliers.js',
                            '/partials/services/templates.js',
                            '/partials/app/quotes/add.js'
                            ]);
                    },
                    next : function (load, quotes) {
                        return quotes.next().then(function (res) {
                            return res.data.response;
                        });
                    },
                    loadClients : function (load, clients) {
                        return clients.load();
                    },
                    loadGroups : function (load, groups) {
                        return groups.load();
                    },
                    loadSuppliers : function (load, suppliers) {
                        return suppliers.load();
                    },
                    loadTemplates : function (load, templates) {
                        return templates.load({id_template: '5876a251056f297b4d415ced'});
                    }
                },
                controller : 'addQuotesCtrl'
            })
            .state('app.quoteDetail', {
                url : '/quotes/{id:string}',
                templateUrl : '/partials/app/quotes/detail.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/dropzone.js',
                            '/partials/directives/datePicker.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/filters/expirationDateFormatFtr.js',
                            '/partials/filters/fromDataBaseFormatTimeFtr.js',
                            '/partials/filters/tsToHour.js',
                            '/partials/services/clients.js',
                            '/partials/services/files.js',
                            '/partials/services/quotes.js',
                            '/partials/services/quote_conditions.js',
                            '/partials/services/quote_log.js',
                            '/partials/services/quote_parts.js',
                            '/partials/services/quote_receipts.js',
                            '/partials/services/quote_status.js',
                            '/partials/services/suppliers.js',
                            '/partials/app/quotes/detail.js'
                        ]);
                    },
                    loadClients : function (load, clients, loadQuotes) {
                        return clients.load({id_user : loadQuotes.data.response[0].id_client});
                    },
                    loadFiles : function ($stateParams, load, files) {
                        return files.load({id_quote : $stateParams.id});
                    },
                    loadQuoteLogs : function ($stateParams, load, quote_log) {
                        return quote_log.load($stateParams.id);
                    },
                    loadQuoteConditions : function (load, quote_conditions) {
                        return quote_conditions.load();
                    },
                    loadQuoteParts : function ($stateParams, load, quote_parts) {
                        return quote_parts.load($stateParams.id);
                    },
                    loadQuoteReceipts : function ($stateParams, load, quote_receipts) {
                        return quote_receipts.load($stateParams.id);
                    },
                    loadQuoteStatus : function (load, quote_status) {
                        return quote_status.load();
                    },
                    loadQuotes : function ($stateParams, $state, load, quotes) {
                        return quotes.load({id_quote : $stateParams.id}).then(function (res) {
                            // Validate quote exists
                            if (res.data.response.length == 0) {
                                $state.go('app.home');
                            }
                            return res;
                        });
                    },
                    loadSuppliers : function (load, suppliers) {
                        return suppliers.load();
                    }
                },
                controller : 'quoteDetailCtrl'
            })
            .state('app.quotes', {
                url : '/quotes',
                templateUrl : '/partials/app/quotes/list.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/datePicker.js',
                            '/partials/directives/tags.js',
                            '/partials/filters/quoteDateFtr.js',
                            '/partials/filters/quoteStatusFtr.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/services/clients.js',
                            '/partials/services/quotes.js',
                            '/partials/services/quote_status.js',
                            '/partials/app/quotes/list.js'
                            ]);
                    },
                    loadQuoteStatus : function (load, quote_status) {
                        return quote_status.load();
                    },
                    loadQuotes : function (load, quotes) {
                        return quotes.list();
                    }
                },
                controller : 'quotesCtrl'
            })
            .state('app.clients', {
                url : '/clients',
                templateUrl : '/partials/app/setup/clients.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/tags.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/services/users.js',
                            '/partials/app/setup/clients.js'
                            ]);
                    },
                    loadUsers : function (load, users) {
                        return users.load({id_user_type : '587434b4056f2914ce7f2e29'});
                    }
                },
                controller : 'clientsCtrl'
            })
            .state('app.groups', {
                url : '/groups',
                templateUrl : '/partials/app/setup/groups.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/multiSelect.js',
                            '/partials/directives/tags.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/services/users.js',
                            '/partials/services/groups.js',
                            '/partials/services/users_groups.js',
                            '/partials/app/setup/groups.js'
                            ]);
                    },
                    loadGroups : function (load, groups) {
                        return groups.load();
                    },
                    loadUsers : function (load, users) {
                        return users.load({id_user_type : '587434b4056f2914ce7f2e28'});
                    },
                    loadUsersGroups : function (load, users_groups) {
                        return users_groups.load();
                    }
                },
                controller : 'groupsCtrl'
            })
            .state('app.labels', {
                url : '/labels',
                templateUrl : '/partials/app/setup/labels.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/tags.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/services/labels.js',
                            '/partials/services/locales.js',
                            '/partials/app/setup/labels.js'
                            ]);
                    },
                    loadLabels : function (load, labels) {
                        return labels.load();
                    }
                },
                controller : 'labelsCtrl'
            })
            .state('app.suppliers', {
                url : '/suppliers',
                templateUrl : '/partials/app/setup/suppliers.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/directives/multiSelect.js',
                            '/partials/directives/tags.js',
                            '/partials/services/users.js',
                            '/partials/services/groups.js',
                            '/partials/services/users_groups.js',
                            '/partials/app/setup/suppliers.js'
                            ]);
                    },
                    loadUsers : function (load, users) {
                        return users.load({id_user_type : '587434b4056f2914ce7f2e28'});
                    },
                    loadGroups : function (load, groups) {
                        return groups.load();
                    },
                    loadUsersGroups : function (load, users_groups) {
                        return users_groups.load();
                    },
                },
                controller : 'suppliersCtrl'
            })
            .state('app.templates', {
                url : '/templates',
                templateUrl : '/partials/app/setup/templates.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/tags.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/services/templates.js',
                            '/partials/services/template_types.js',
                            '/partials/services/locales.js',
                            '/partials/app/setup/templates.js'
                            ]);
                    },
                    loadTemplates : function (load, templates) {
                        return templates.load();
                    },
                    loadTemplateTypes : function (load, template_types) {
                        return template_types.load();
                    }
                },
                controller : 'templatesCtrl'
            })
            .state('app.users', {
                url : '/users',
                templateUrl : '/partials/app/setup/users.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/tags.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/services/users.js',
                            '/partials/app/setup/users.js'
                            ]);
                    },
                    loadUsers : function (load, users) {
                        return users.load({id_user_type : '587434b4056f2914ce7f2e27'});
                    }
                },
                controller : 'usersCtrl'
            })
            .state('app.logs', {
                url : '/logs',
                templateUrl : '/partials/app/setup/logs.html',
                resolve : {
                    load : function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            '/partials/directives/tags.js',
                            '/partials/filters/fromDataBaseFormatFtr.js',
                            '/partials/filters/tsToHour.js',
                            '/partials/services/logs.js',
                            '/partials/services/logs_uuid.js',
                            '/partials/app/setup/logs.js'
                            ]);
                    },
                    loadUsers : function (load, logs_uuid) {
                        return logs_uuid.load();
                    }
                },
                controller : 'logsCtrl'
            });

        $urlRouterProvider.otherwise('/login');

        $httpProvider.interceptors.push('httpService');
}]);

/**
 * Auth Service
 */

angular.module('app').factory(
    'authService',
    function () {
        return {
            isAuthenticated : function () {
                return $.localStorage.isSet('sid');
            },
            getSID : function () {
                return $.localStorage.get('sid');
            },
            setSID : function (sid) {
                return $.localStorage.set('sid', sid);
            },
            clear : function () {
                return $.localStorage.remove('sid');
            },
            clearAll : function () {
                return $.localStorage.removeAll();
            }
        };
});

/**
 * HTTP Service Interceptor
 */

angular.module('app').factory(
    'httpService',
    function ($injector, $q, authService, ASSETS) {
        return {
            request : function (res) {
                var parseURL = res.url.split('/');
                if (parseURL.length >= 1 && parseURL[1] === 'api') {
                    parseURL.splice(1, 1);
                    res.url = ASSETS.apiURL + parseURL.join('/');
                }
                if (authService.getSID()) {
                    res.headers['Authorization'] = authService.getSID();
                }
                res.headers['X-Requested-With'] = 'XMLHttpRequest';

                return res;
            },
            responseError : function (res) {
                if (res.status === 401) {
                    authService.clear();
                    $injector.invoke(function ($state) {
                        $state.go('login');
                    });
                } else if (res.status === 400) {
                    $injector.invoke(function (notify) {
                        notify({messageTemplate:'<span><b>Error:</b> '+res.data.error+'</span>', classes:'alert alert-warning'});
                    });
                }

                return $q.reject(res);
            }
        };
});

/**
 * Collection
 */

angular.module('app').factory(
    'collection',
    function () {
        return {
            add : function (collection, row, field) {
                var found = false;
                for (var key = 0; key < collection.length; key++) {
                    if (collection[key][field] == row[field]) {
                        found = true;
                        collection[key] = row;
                    }
                }

                if (!found) {
                    collection.push(row);
                }

                return collection;
            },
            remove : function (collection, id, field) {
                for (var key = 0; key < collection.length; ++key) {
                        if (collection[key][field] === id) {
                            collection.splice(key, 1);
                        }
                    }
                return collection;
            }
        };
});

/**
 * Numeric
 */

angular.module('app').directive(
    'numeric',
    function () {
        return {
            require : 'ngModel',
            link : function (scope, element, attr, ngModelCtrl) {
                function fromUser (text) {
                    var transformedInput = text.replace(/[^0-9.]/g, '');
                    if(transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
});

/**
 * On enter
 */

angular.module('app').directive(
    'onEnter',
    function () {
        return function (scope, element, attrs) {
            element.bind('keydown keypress', function (event) {
                if (event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.onEnter);
                    });

                    event.preventDefault();
                }
            });
        };
});