angular.module('app').service(
    'template_types',
    function (
        $http,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_template_type';
        };

        obj.baseURL = function (id) {
            return (id) ? '/api/catalogues/template_types/'+id : '/api/catalogues/template_types';
        };

        /**
         * Get all
         */

        obj.load = function (params) {
            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };

        /**
         * Delete method
         */

        obj.delete = function (id) {
            return $http.delete(obj.baseURL()+'/'+id).then(function (res) {
                collection.remove(obj.collection, id, obj.id());

                return res;
            });
        };

        /**
         * Save method
         */

        obj.save = function (row) {
            var config = {
                data   : row,
                method : (row[obj.id()]) ? 'PUT' : 'POST',
                url    : obj.baseURL(row[obj.id()])
            };

            return $http(config).then(function (res) {
                collection.add(obj.collection, res.data.response, obj.id());

                return res;
            });
        };
});
