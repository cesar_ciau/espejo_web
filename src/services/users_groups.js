angular.module('app').service(
    'users_groups',
    function (
        $http,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_user_group';
        };

        obj.baseURL = function (id_user, id_group) {
            if (id_user && id_group) {
                return '/api/users/'+id_user+'/groups/'+id_group;
            } else if (id_user) {
                return '/api/users/'+id_user+'/groups';
            } else if (id_group) {
                return '/api/users/groups/'+id_group;
            } else {
                return '/api/users/groups';
            }
        };

        /**
         * Load collection
         */

        obj.load = function (params) {
            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };

        /**
         * Delete method
         */

        obj.delete = function (id_user, id_group) {
            return $http.delete(obj.baseURL(id_user, id_group)).then(function (res) {
                collection.remove(obj.collection, id, obj.id());

                return res;
            });
        };

        /**
         * Save method
         */

        obj.save = function (row) {
            var config = {
                data   : row,
                method : (row[obj.id()]) ? 'PUT' : 'POST',
                url    : obj.baseURL(row['id_user'], row['id_group'])
            };

            return $http(config).then(function (res) {
                collection.add(obj.collection, row, obj.id());

                return res;
            });
        };

        /**
         * Custom methods
         */

        obj.match = function (params) {
            return $http.get(obj.baseURL()+'/match', {params : params}).then(function (res) {
                return res;
            });
        };
});
