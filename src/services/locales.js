angular.module('app').service(
    'locales',
    function () {

        var obj = this;

        obj.collection = [
            {'code' : 'en', 'name' : 'Ingles (en)'},
            {'code' : 'es', 'name' : 'Español (es)'}
        ];

});
