angular.module('app').service(
    'logs_uuid',
    function (
        $http,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_log_uuid';
        };

        obj.baseURL = function (id) {
            return (id) ? '/api/logs/uuid/'+id : '/api/logs/uuid';
        };

        /**
         * Load collection
         */

        obj.load = function (params) {
            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };
});
