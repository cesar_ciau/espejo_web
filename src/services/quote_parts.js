angular.module('app').service(
    'quote_parts',
    function (
        $http,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_quote_part';
        };

        obj.baseURL = function (id_quote, id) {
            return (id) ? '/api/quotes/'+id_quote+'/part_no/'+id : '/api/quotes/'+id_quote+'/part_no';
        };

        /**
         * Get all
         */

        obj.load = function (id_quote, params) {
            return $http.get(obj.baseURL(id_quote), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };

        /**
         * Delete method
         */

        obj.delete = function (id_quote, id) {
            return $http.delete(obj.baseURL(id_quote, id)).then(function (res) {
                collection.remove(obj.collection, id, obj.id());

                return res;
            });
        };

        /**
         * Save method
         */

        obj.save = function (row) {
            var config = {
                data   : row,
                method : (row[obj.id()]) ? 'PUT' : 'POST',
                url    : obj.baseURL(row[obj.id()])
            };

            return $http(config).then(function (res) {
                collection.add(obj.collection, res.data.response, obj.id());

                return res;
            });
        };
});
