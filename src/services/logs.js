angular.module('app').service(
    'logs',
    function (
        $http,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_log';
        };

        obj.baseURL = function (id) {
            return (id) ? '/api/logs/'+id : '/api/logs';
        };

        /**
         * Load collection
         */

        obj.load = function (params) {
            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };
});
