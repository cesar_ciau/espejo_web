angular.module('app').service(
    'clients',
    function (
        $http,
        $ocLazyLoad,
        ASSETS,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_user';
        };

        obj.baseURL = function (id_user) {
            return (id_user) ? '/api/users/'+id_user : '/api/users';
        };

        /**
         * Load Collection
         */

        obj.load = function (params) {
            if (!angular.isObject(params)) {
                params = {};
            }

            params.id_user_type = '587434b4056f2914ce7f2e29';

            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };

        /**
         * Delete method
         */

        obj.delete = function (id) {
            return $http.delete(obj.baseURL(id)).then(function (res) {
                collection.remove(obj.collection, id, obj.id());

                return res;
            });
        };

        /**
         * Save method
         */

        obj.save = function (row) {
            var config = {
                data   : row,
                method : (row[obj.id()]) ? 'PUT' : 'POST',
                url    : obj.baseURL(row[obj.id()])
            };

            // Assigns a random password if non provided
            if (config.method === 'POST') {
                if (config.data.password === null || config.data.password === undefined) {
                    config.data.password = Math.random();
                }
            }

            // Encrypts user password
            if (config.data.password !== null && config.data.password !== undefined) {
                config.data.password = obj.hash(config.data.password);
            } else {
                delete config.data.password;
            }

            return $http(config).then(function (res) {
                collection.add(obj.collection, res.data.response, obj.id());

                return res;
            });
        };

        /**
         * Hash method (password)
         */

        obj.hash = function (string) {
            console.warn('Hash function not loaded');
        };

        $ocLazyLoad.load(ASSETS.jsSHA).then(function () {
            obj.hash = function (string) {
                var shaObj = new jsSHA(string, 'TEXT');
                return shaObj.getHash('SHA-512', 'HEX');
            };
        });

        /**
         * Get tooltip
         */

        obj.getToolTip = function (row) {
            var tooltip = angular.copy(row.username);

            if (row.name !== null && row.name.length > 0) {
                tooltip += '<br>'+angular.copy(row.name);
            }

            if (row.phone !== null && row.phone.length > 0) {
                tooltip += '<br>'+angular.copy(row.phone);
            }

            if (row.phone2 !== null && row.phone2.length > 0) {
                tooltip += '<br>'+angular.copy(row.phone2);
            }

            if (row.address !== null && row.address.length > 0) {
                tooltip += '<br>'+angular.copy(row.address);
            }

            return tooltip;
        };
});
