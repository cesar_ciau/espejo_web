angular.module('app').service(
    'quotes',
    function (
        $http,
        collection
    ) {

        var obj = this;

        obj.collection = [];

        /**
         * Constants
         */

        obj.id = function () {
            return 'id_quote';
        };

        obj.baseURL = function (id) {
            return (id) ? '/api/quotes/'+id : '/api/quotes';
        };

        /**
         * Load collection
         */

        obj.load = function (params) {
            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        };

        obj.get = function (params) {
            return $http.get(obj.baseURL(), {params: params}).then(function (res) {
                return res;
            });
        };

        /**
         * Delete method
         */

        obj.delete = function (id) {
            return $http.delete(obj.baseURL()+'/'+id).then(function (res) {
                collection.remove(obj.collection, id, obj.id());

                return res;
            });
        };

        /**
         * Save method
         */

        obj.save = function (row) {
            var config = {
                data   : row,
                method : (row[obj.id()]) ? 'PUT' : 'POST',
                url    : obj.baseURL(row[obj.id()])
            };

            return $http(config).then(function (res) {
                collection.add(obj.collection, res.data.response, obj.id());

                return res;
            });
        };

        /**
         * Custom methods
         */

        obj.list = function () {
            return $http.get(obj.baseURL()+'/list').then(function (res) {
                obj.collection = res.data.response;

                return res;
            });
        }

        obj.next = function () {
            return $http.get(obj.baseURL()+'/next').then(function (res) {

                return res;
            });
        };

        obj.preview = function (markup) {
            return $http.post('/api/quotes/parsePartNumber', {markup: markup}).then(function (res) {
                return res;
            });
        };
});
