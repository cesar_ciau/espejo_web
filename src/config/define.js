angular.module('app', [
    'angular-loading-bar',
    'cgNotify',
    'checklist-model',
    'easypiechart',
    'ngAnimate',
    'ngTable',
    'oc.lazyLoad',
    'textAngular',
    'ui.router',
]);