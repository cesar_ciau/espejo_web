/**
 * Constant
 */

angular.module('app').constant('ASSETS', {
    apiURL : 'http://api2-development.meditegic.com/erp',
    jsSHA : '/assets/vendor/jssha/sha512.min.js'
});