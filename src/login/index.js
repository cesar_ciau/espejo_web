angular.module('app').controller(
    'loginCtrl',
    function (
        $http,
        $scope,
        $state,
        authService
    ) {

    authService.clearAll();

    var d = new Date();
    $scope.year = d.getFullYear();

    $scope.css = {
        submit : 'btn-primary'
    };

    $scope.data = {
        username : '',
        password : ''
    };

    $scope.doLogin = function () {
        var shaObj = new jsSHA($scope.data.password, 'TEXT');

        $http
            .post('/api/sessions', {
                username : $scope.data.username,
                password : shaObj.getHash('SHA-512', 'HEX')
            })
            .success(function (data) {
                authService.setSID(data.response.token);
                $.localStorage.set('username', $scope.data.username);
                $scope.css.submit = 'btn-success';
                $state.go('app.quotes');
                // $state.go('app.home');
            })
            .error(function (data) {
                $('#user').focus();
                $scope.data.password = '';
                $scope.css.submit = 'btn-danger';
            });
    };
});
