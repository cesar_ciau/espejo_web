angular.module('app').filter(
    'tsToHour',
    function () {
        return function (data) {
            return (data) ? moment(data * 1000).format('hh:mm:ss a') : null;
        };
});
