angular.module('app').filter(
    'expirationDateFormatFtr',
    function () {
        return function (data) {
        	//var date = (data) ? moment(data*1000).format('DD-MM-YYYY') : null;
        	return moment().add(7, 'days').format('DD-MM-YYYY');
        };
});
