angular.module('app').filter(
    'quoteDateFtr',
    function () {
        return function (data, field, values) {
            var nameField;
            var dt_from = (values.from !== null) ? parseInt(moment(values.from).format('YYYYMMDD')) : null;
            var dt_to = (values.to !== null) ? parseInt(moment(values.to).format('YYYYMMDD')) : null;

            var data2 = [];
            for (var i = data.length - 1; i >= 0; i--) {
                if (typeof field == "string") {
                    nameField = field;
                }
                if (field == "dt_payment") {
                    if (data[i]["dt_payment"] !== null) {
                        nameField = "dt_payment";
                    }
                    if (data[i]["dt_payment_llc"] !== null) {
                        nameField = "dt_payment_llc";
                    }
                    if (data[i]["dt_payment_sa"] !== null) {
                        nameField = "dt_payment_sa";
                    }
                }
                var dt_create = parseInt(moment(data[i][nameField] * 1000).format('YYYYMMDD'));
                var found = false;

                if (dt_from === null && dt_to === null) {
                    found = true;
                } else if(dt_from !== null && dt_to !== null) {
                    if (dt_create >= dt_from && dt_create <= dt_to) {
                        found = true;
                    }
                } else if (dt_create >= dt_from && dt_from !== null) {
                    found = true;
                } else if (dt_create <= dt_to && dt_to !== null) {
                    found = true;
                }
                if (found) {
                    data2.push(data[i]);
                }
            }

            return data2;
        };
});