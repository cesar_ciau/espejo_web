angular.module('app').filter(
    'fromDataBaseFormatFtr',
    function () {
        return function (data) {
            return (data) ? moment(data*1000).format('DD-MM-YYYY') : null;
        };
});
