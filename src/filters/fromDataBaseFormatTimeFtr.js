angular.module('app').filter(
    'fromDataBaseFormatTimeFtr',
    function () {
        return function (data) {
            return (data) ? moment(data*1000).format('DD-MM-YYYY hh:mm:ss a') : null;
        };
});
