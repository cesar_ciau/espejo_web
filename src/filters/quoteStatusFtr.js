angular.module('app').filter(
    'quoteStatusFtr',
    function () {
        return function (data, filter) {
            if (filter.length == 0) {
                return data;
            }

            var data2 = [];
            for (var i = data.length - 1; i >= 0; i--) {
                for (var j = filter.length - 1; j >= 0; j--) {
                    if (data[i].id_quote_status == filter[j]) {
                        data2.push(data[i]);
                    }
                }
            }
            return data2;
        };

});
