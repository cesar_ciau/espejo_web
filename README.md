# Web

Repositorio de [erp.meditegic.com](http://erp.meditegic.com)

--------

### Herramientas

- [nodejs](http://nodejs.org)
- [gulpjs](http://gulpjs.com/)

### Instalación

Instalar localmente gulp

```
$ npm install gulp
```

Instalar paquetes

```
$ npm install .
```

### Task runner

Para que los cambios se reflejen hay que ejecutar gulp en la linea de comandos

```
$ gulp
```