/**
 * Gulp tasks definition
 */

var gulp = require('gulp');
var changed = require('gulp-changed');
var del = require('del');
var minifyHTML = require('gulp-htmlmin');
var minifyCSS = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
 
/**
 * Path configuration
 */

var config = {
    css : {
        dest : 'public/partials',
        src  : [
            'src/*.css',
            'src/*/*.css',
            'src/*/*/*.css'
        ]
    },
    js : {
        dest : 'public/partials',
        src  : [
            'src/*.js',
            'src/*/*.js',
            'src/*/*/*.js',
        ]
    },
    html : {
        dest : 'public/partials',
        src  : [
            'src/*.html',
            'src/*/*.html',
            'src/*/*/*.html',
            'src/*/*/*/*.html',
        ]
    }
};
 
/**
 * Remove markup, stlyes and scripts
 */

gulp.task('cleanup', function () {
    var dir = [
        config.html.dest,
        config.js.dest,
        config.css.dest,
    ];

    return del.sync(dir, {force: true});
});
 
/**
 * Minifies css
 */

gulp.task('css', function () {
    gulp.src(config.css.src)
        .pipe(changed(config.css.dest))
        .pipe(minifyCSS())
        .pipe(gulp.dest(config.css.dest));
});

 
/**
 * Minifies js
 */

gulp.task('js', function () {
    gulp.src(config.js.src)
        .pipe(changed(config.js.dest))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.js.dest))
        .pipe(sourcemaps.init());
});

 
/**
 * Minifies html
 */

gulp.task('html', function () {
    var options = {
        removeComments : true,
        removeCommentsFromCDATA : true,
        collapseWhitespace : true,
        conservativeCollapse : true,
        collapseInlineTagWhitespace : true,
        caseSensitive : true,
        removeAttributeQuotes : true,
        removeEmptyAttributes : true,
        removeScriptTypeAttributes : true,
        removeStyleLinkTypeAttributes : true,
        keepClosingSlash : true
    };

    gulp.src(config.html.src)
        .pipe(changed(config.html.dest))
        .pipe(minifyHTML(options))
        .pipe(gulp.dest(config.html.dest));
});
 
/**
 * Bootstrap all tasks
 */

gulp.task('default', ['cleanup'], function () {
    // Start process after cleanup
    gulp.start('css', 'js', 'html');
 
    // Watchers
    gulp.watch(config.css.src, ['css']);
    gulp.watch(config.js.src, ['js']);
    gulp.watch(config.html.src, ['html']);
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(config.css.src, ['css']);
    gulp.watch(config.js.src, ['js']);
    gulp.watch(config.html.src, ['html']);
});
